module carrier_nco(
    input clk,
    input reset,
    input [29:0] init_theta,
    input [29:0] theta_step,
    input tick,
    output signed [2:0] sin,
    output signed [2:0] cos,
    output reg sin_cos_valid
    );

    initial sin_cos_valid=1'b0;

    reg [29:0] theta='d0;
    sin_cos_table lookup(.clk(clk), .theta(theta[29:26]), .sin(sin), .cos(cos));

    always @(posedge clk)
    begin
        if(!reset)
        begin
            theta <= init_theta;
            sin_cos_valid <= 1'b0;
        end
        else
        begin
            if(tick)
                theta <= theta + theta_step;

            sin_cos_valid <= tick;
        end
    end

endmodule

