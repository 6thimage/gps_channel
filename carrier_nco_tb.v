`timescale 1 ns/1 ps

module carrier_nco_tb();
    /* inputs */
    reg clk, reset;
    reg [29:0] init_theta, theta_step;
    reg tick;

    /* outputs */
    wire signed [2:0] sin, cos;
    wire sin_cos_valid;

    carrier_nco uut(.clk(clk), .reset(reset), .init_theta(init_theta), .theta_step(theta_step), .tick(tick),
                    .sin(sin), .cos(cos), .sin_cos_valid(sin_cos_valid));

    always #0.5 clk <= ~clk;

    integer i;
    initial
    begin
        reset=0;
        tick=0;
        clk=1;
        init_theta=0;
        theta_step=29'd268487411;

        #100;
        /* remove from reset */
        reset=1;

        for(i=0; i<100000; i=i+1)
        begin
            #1 tick=0;
            #1 tick=1;
        end

        #20 $finish;
    end

    always @(posedge clk)
    begin
        if(sin_cos_valid)
            $display("s %d c %d", sin, cos);
    end

endmodule

