/* verilator lint_off WIDTH */
module constant_mul_combined #(
    parameter short_width=20,
    parameter long_width=20,
    parameter bit_shift=10
    )
    (
    input clk,
    input reset,
    input enable,
    input short,
    input signed [short_width-bit_shift-1:0] short_in,
    input signed [short_width-bit_shift-1:0] short_const,
    output reg signed [short_width-bit_shift-1:0] short_out,
    output reg short_out_valid,
    input signed [long_width-bit_shift-1:0] long_in,
    input signed [long_width-bit_shift-1:0] long_const,
    output reg signed [long_width-bit_shift-1:0] long_out,
    output reg long_out_valid
    );

    initial
    begin
        short_out_valid=1'b0;
        long_out_valid=1'b0;
    end

    reg signed [17:0] mul_al, mul_ah, mul_bl, mul_bh;

    always @*
    begin
        /* setup for long */
        mul_al = {1'b0, long_in[16:0]};
        mul_ah = long_in>>>17;
        mul_bl = {1'b0, long_const[16:0]};
        mul_bh = long_const>>>17;

        if(short)
        begin
            mul_al = short_in;
            mul_bl = short_const;
        end
    end

    reg signed [35:0] mul_cll, mul_clh, mul_chl, mul_chh;
    wire signed [69:0] mul_c=(mul_chh<<<34) + ((mul_clh+mul_chl)<<<17) + mul_cll;
    reg stage_0=1'b0;

    wire signed [short_width-1:0] short_result=mul_cll;
    reg signed [long_width-1:0] long_result;
    always @(posedge clk)
    begin
        if(!reset)
        begin
            short_out_valid <= 1'b0;
            long_out_valid <= 1'b0;
            stage_0 <= 1'b0;
        end
        else
        begin
            short_out_valid <= 1'b0;

            if(enable)
            begin
                mul_cll <= mul_al*mul_bl;

                if(short)
                    short_out_valid <= 1'b1;
                else
                begin
                    mul_clh <= mul_al*mul_bh;
                    mul_chl <= mul_ah*mul_bl;
                    mul_chh <= mul_ah*mul_bh;
                end
            end

            if(stage_0)
                long_result <= mul_c;

            {long_out_valid, stage_0} <= {stage_0, enable&(!short)};
        end
    end

    always @(*)
    begin
        short_out = short_result[short_width-1:bit_shift];
        long_out = long_result[long_width-1:bit_shift];
    end

endmodule

