/* verilator lint_off WIDTH */
module constant_mul_wide #(
    parameter mul_width=20,
    parameter bit_shift=10
    )
    (
    input clk,
    input reset,
    input enable,
    input signed [mul_width-bit_shift-1:0] mul_in,
    input signed [mul_width-bit_shift-1:0] mul_const,
    output reg signed [mul_width-bit_shift-1:0] mul_out,
    output reg out_valid
    );

    initial out_valid=1'b0;

    wire signed [17:0] mul_al={1'b0, mul_in[16:0]},
                       mul_ah=mul_in>>>17,
                       mul_bl={1'b0, mul_const[16:0]},
                       mul_bh=mul_const>>>17;
    reg signed [35:0] mul_cll, mul_clh, mul_chl, mul_chh;
    wire signed [69:0] mul_c=(mul_chh<<<34) + ((mul_clh+mul_chl)<<<17) + mul_cll;
    reg stage0=1'b0;

    reg signed [mul_width-1:0] mul_result;
    always @(posedge clk)
    begin
        if(!reset)
        begin
            out_valid <= 1'b0;
            stage0 <= 1'b0;
        end
        else
        begin
            if(enable)
            begin
                mul_cll <= mul_al*mul_bl;
                mul_clh <= mul_al*mul_bh;
                mul_chl <= mul_ah*mul_bl;
                mul_chh <= mul_ah*mul_bh;
            end

            if(stage0)
                mul_result <= mul_c;

            {out_valid, stage0} <= {stage0, enable};
        end
    end

    always @(*)
        mul_out = mul_result >>> bit_shift;

endmodule

