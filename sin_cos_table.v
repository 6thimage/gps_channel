module sin_cos_table(
    input clk,
    input [3:0] theta,
    output reg signed [2:0] sin,
    output reg signed [2:0] cos
    );

    always @(posedge clk)
    begin
        sin <= sin_lookup(theta);
        cos <= cos_lookup(theta);
    end

    function signed [2:0] sin_lookup(input [3:0] angle);
        begin
            case(angle)
            0: sin_lookup = 3'd0;
            1: sin_lookup = 3'd1;
            2: sin_lookup = 3'd2;
            3: sin_lookup = 3'd3;
            4: sin_lookup = 3'd3;
            5: sin_lookup = 3'd3;
            6: sin_lookup = 3'd2;
            7: sin_lookup = 3'd1;
            8: sin_lookup = 3'd0;
            9: sin_lookup = -3'd1;
            10: sin_lookup = -3'd2;
            11: sin_lookup = -3'd3;
            12: sin_lookup = -3'd3;
            13: sin_lookup = -3'd3;
            14: sin_lookup = -3'd2;
            15: sin_lookup = -3'd1;
            endcase
        end
    endfunction

    function signed [2:0] cos_lookup(input [3:0] angle);
        begin
            case(angle)
            0: cos_lookup = 3'd3;
            1: cos_lookup = 3'd3;
            2: cos_lookup = 3'd2;
            3: cos_lookup = 3'd1;
            4: cos_lookup = 3'd0;
            5: cos_lookup = -3'd1;
            6: cos_lookup = -3'd2;
            7: cos_lookup = -3'd3;
            8: cos_lookup = -3'd3;
            9: cos_lookup = -3'd3;
            10: cos_lookup = -3'd2;
            11: cos_lookup = -3'd1;
            12: cos_lookup = 3'd0;
            13: cos_lookup = 3'd1;
            14: cos_lookup = 3'd2;
            15: cos_lookup = 3'd3;
            endcase
        end
    endfunction

endmodule

