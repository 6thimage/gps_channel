module magnitude_estimator #(
    parameter width=20
    )
    (
    input clk,
    input enable,
    input signed [width-1:0] I, Q,
    output reg [width+1:0] magnitude,
    output reg output_valid
    );

    initial output_valid=1'b0;

    /* from http://dspguru.com/book/export/html/62
     *
     * Magnitude \approx Alpha * max(|I|, |Q|) + Beta * min(|I|, |Q|)
     * =====================================================================
     * Name                  Alpha           Beta       Avg Err   RMS   Peak
     *                                                  (linear)  (dB)  (dB)
     * ---------------------------------------------------------------------
     * Min RMS Err      0.947543636291 0.392485425092   0.000547 -32.6 -25.6
     * Min Peak Err     0.960433870103 0.397824734759  -0.013049 -31.4 -28.1
     * Min RMS w/ Avg=0 0.948059448969 0.392699081699   0.000003 -32.6 -25.7
     * 1, Min RMS Err   1.000000000000 0.323260990000  -0.020865 -28.7 -23.8
     * 1, Min Peak Err  1.000000000000 0.335982538000  -0.025609 -28.3 -25.1
     * 1, 1/2           1.000000000000 0.500000000000  -0.086775 -20.7 -18.6
     * 1, 1/4           1.000000000000 0.250000000000   0.006456 -27.6 -18.7
     * Frerking         1.000000000000 0.400000000000  -0.049482 -25.1 -22.3
     * 1, 11/32         1.000000000000 0.343750000000  -0.028505 -28.0 -24.8
     * 1, 3/8           1.000000000000 0.375000000000  -0.040159 -26.4 -23.4
     * 15/16, 15/32     0.937500000000 0.468750000000  -0.018851 -29.2 -24.1
     * 15/16, 1/2       0.937500000000 0.500000000000  -0.030505 -26.9 -24.1
     * 31/32, 11/32     0.968750000000 0.343750000000  -0.000371 -31.6 -22.9
     * 31/32, 3/8       0.968750000000 0.375000000000  -0.012024 -31.4 -26.1
     * 61/64, 3/8       0.953125000000 0.375000000000   0.002043 -32.5 -24.3
     * 61/64, 13/32     0.953125000000 0.406250000000  -0.009611 -31.8 -26.6
     * =====================================================================
     *
     * here we are choosing the (1, 1/4) as it is the best (lowest error) that
     * can be done by only shifting (i.e. no constant multipliers)
     */

    /* sign removal
     * we keep the same length here as the signed data, although the MSb is always 0,
     * to stop the synthesis tool from bitching
     */
    reg signed [width-1:0] I_unsigned, Q_unsigned;
    always @(*)
    begin
        if(I<0)
            I_unsigned = -I;
        else
            I_unsigned = I;

        if(Q<0)
            Q_unsigned = -Q;
        else
            Q_unsigned = Q;
    end

    /* min and max */
    reg [width-1:0] min, max;
    always @(*)
    begin
        if(I_unsigned > Q_unsigned)
        begin
            max = $unsigned(I_unsigned);
            min = $unsigned(Q_unsigned);
        end
        else
        begin
            max = $unsigned(Q_unsigned);
            min = $unsigned(I_unsigned);
        end
    end

    /* magnitude estimation */
    always @(posedge clk)
    begin
        output_valid <= 1'b0;

        if(enable)
        begin
            /* equivalent to (max<<2) + min, but ensures widths are correct */
            magnitude <= {max, 2'd0} + {2'd0, min};
            output_valid <= 1'b1;
        end
    end
endmodule

