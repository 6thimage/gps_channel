module lock_detector #(
    parameter prompt_width=16
    )
    (
    input clk,
    input reset,
    input prompt_valid,
    input signed [prompt_width-1:0] prompt_I, prompt_Q,
    output reg optimistic_lock,
    output reg pessimistic_lock
    );

    initial
    begin
        optimistic_lock=1'b0;
        pessimistic_lock=1'b0;
    end

    /* algorithm
     * - accumulate prompt phase over 20 code periods (20 ms)
     * - low pass filter of current phase and last
     * - reduce I component by constant
     * - compare I and Q
     *     - if I > Q:
     *         - lock_false = 0
     *         - optimistic_lock = 1
     *         - if lock_true > 50:
     *             - pessimistic_lock = 1
     *         - else:
     *             - ++lock_true
     *     - else:
     *         - lock_true = 0
     *         - pessimistic_lock = 0
     *         - if lock_false > 240:
     *             - optimistic_lock = 0
     *         - else:
     *             - ++lock_false
     */

    localparam accum_width=prompt_width+5;
    localparam lp_width=accum_width+10;

    /* accumulation */
    reg [4:0] accum_counter='d0;
    reg signed [accum_width-1:0] accum_I='d0, accum_Q='d0;

    reg lp_in_valid=1'b0;
    reg signed [accum_width-1:0] lp_in_s_I='d0, lp_in_s_Q='d0;

    always @(posedge clk)
    begin
        if(!reset)
        begin
            accum_counter <= 'd0;
            accum_I <= 'd0;
            accum_Q <= 'd0;

            lp_in_valid <= 1'b0;
        end
        else
        begin
            lp_in_valid <= 1'b0;

            /* accumulation of input */
            if(prompt_valid)
            begin
                accum_counter <= accum_counter + 1'b1;
                accum_I <= accum_I + {{5{prompt_I[prompt_width-1]}}, prompt_I};
                accum_Q <= accum_Q + {{5{prompt_Q[prompt_width-1]}}, prompt_Q};
            end

            /* check accumulator count */
            if(accum_counter=='d20)
            begin
                /* output */
                lp_in_valid <= 1'b1;
                lp_in_s_I <= accum_I;
                lp_in_s_Q <= accum_Q;

                /* reset accumulators */
                accum_counter <= 'd0;
                accum_I <= 'd0;
                accum_Q <= 'd0;
            end
        end
    end

    /* sign removal of lp_in */
    reg [accum_width-1:0] lp_in_I='d0, lp_in_Q='d0;
    always @*
    begin
        lp_in_I = lp_in_s_I;
        lp_in_Q = lp_in_s_Q;

        if(lp_in_s_I < 0)
            lp_in_I = -lp_in_I;

        if(lp_in_s_Q < 0)
            lp_in_Q = -lp_in_Q;
    end

    /* low pass filtering
     *
     * filtered = k1 * (input - last_input) + last_input
     * I scaling - filter_i / k2
     *
     * stages
     * 0    input - last_input
     * 1    k1 * stage0
     * 2    stage1 + last_input
     * 3    last_input = input, I scaling
     *
     * constants
     * k1   0.0247
     * k2   1.5
     *
     * using a 10 bit shift for constant multiplication
     * k1        25.2928    ~  25   (1<<4)|(1<<3)|(1<<0)
     * 1/k2     682.6666    ~ 683   (1<<9)|(1<<7)|(1<<5)|(1<<3)|(1<<1)|(1<<0)
     *
     * as there is a large gap inbetween filtering (20 ms), this is
     * best implemented as a state machine, rather than being pipelined
     */
    reg [lp_width-1:0] lp_last_I='d0, lp_last_Q='d0;
    reg [lp_width-1:0] lp_temp_I, lp_temp_Q;

    reg lp_out_valid=1'b0;
    reg [lp_width-1:0] lp_out_I, lp_out_Q;

    reg [1:0] lp_state='d0;

    always @(posedge clk)
    begin
        if(!reset)
        begin
            lp_last_I <= 'd0;
            lp_last_Q <= 'd0;

            lp_out_valid <= 1'b0;
        end
        else
        begin
            lp_out_valid <= 1'b0;

            lp_state <= lp_state + 1'b1;

            case(lp_state)
            0:
            begin
                if(lp_in_valid)
                begin
                    lp_temp_I <= {10'd0, lp_in_I} - lp_last_I;
                    lp_temp_Q <= {10'd0, lp_in_Q} - lp_last_Q;
                end
                else
                    lp_state <= 'd0;
            end
            1:
            begin
                /* the multiplication here is split into bit shifts */
                lp_temp_I <= (lp_temp_I<<4) | (lp_temp_I<<3) | (lp_temp_I);
                lp_temp_Q <= (lp_temp_Q<<4) | (lp_temp_Q<<3) | (lp_temp_Q);
            end
            2:
            begin
                lp_temp_I <= lp_temp_I + lp_last_I;
                lp_temp_Q <= lp_temp_Q + lp_last_Q;
            end
            3:
            begin
                lp_last_I <= lp_temp_I;
                lp_last_Q <= lp_temp_Q;

                lp_out_valid <= 1'b1;
                lp_out_I <= (lp_temp_I<<9) | (lp_temp_I<<7) | (lp_temp_I<<5) |
                            (lp_temp_I<<3) | (lp_temp_I<<1) | (lp_temp_I);
                lp_out_Q <= lp_temp_Q;
            end
            endcase
        end
    end

    /* lock detection update */
    reg [5:0] ld_true='d0;
    reg [7:0] ld_false='d0;

    always @(posedge clk)
    begin
        if(!reset)
        begin
            optimistic_lock <= 1'b0;
            pessimistic_lock <= 1'b0;

            ld_true <= 'd0;
            ld_false <= 'd0;
        end
        else if(lp_out_valid)
        begin
            if(lp_out_I > lp_out_Q)
            begin
                ld_false <= 'd0;
                ld_true <= ld_true + 1'b1;

                optimistic_lock <= 1'b1;
                if(ld_true > 6'd50)
                begin
                    pessimistic_lock <= 1'b1;
                    ld_true <= 6'd51;
                end
            end
            else
            begin
                ld_true <= 'd0;
                ld_false <= ld_false + 1'b1;

                pessimistic_lock <= 1'b0;
                if(ld_false > 8'd240)
                begin
                    optimistic_lock <= 1'b0;
                    ld_false <= 8'd241;
                end
            end
        end
    end

endmodule

