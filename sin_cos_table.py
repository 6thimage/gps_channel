#! /usr/bin/env python3

import numpy as np

tab=4*' '

theta_width=4
sin_cos_width=3

def to_veri_val(width, val):
    sign=''
    val=int(val)
    if(val<0):
        sign='-'
        val=-val
    return "{}{}'d{}".format(sign, width, val)

def function_output(fn_name, fn_width, arg, arg_width, vals):
    print(tab+'function signed [{}:0] {}(input [{}:0] {});'.format(fn_width-1, fn_name, arg_width-1, arg))
    print(2*tab+'begin')
    print(3*tab+'case({})'.format(arg))
    for i in range(0, len(vals)):
        print(3*tab+'{}: {} = {};'.format(i, fn_name, to_veri_val(3, vals[i])))
    print(3*tab+'endcase')
    print(2*tab+'end')
    print(tab+'endfunction')

print('module sin_cos_table(')
print(tab+'input clk,')
print(tab+'input [{}:0] theta,'.format(theta_width-1))
print(tab+'output reg signed [{}:0] sin,'.format(sin_cos_width-1))
print(tab+'output reg signed [{}:0] cos'.format(sin_cos_width-1))
print(tab+');')
print()
print(tab+'always @(posedge clk)')
print(tab+'begin')
print(2*tab+'sin <= sin_lookup(theta);')
print(2*tab+'cos <= cos_lookup(theta);')
print(tab+'end')
print()

x=np.arange(0, 16)
sine=3*np.sin(2*np.pi*x/16)
sine=np.round(sine)
function_output('sin_lookup', sin_cos_width, 'angle', theta_width, sine)
print()

cosine=3*np.cos(2*np.pi*x/16)
cosine=np.round(cosine)
function_output('cos_lookup', sin_cos_width, 'angle', theta_width, cosine)
print()

print('endmodule')
print()

