module constant_mul #(
    parameter mul_width=20,
    parameter bit_shift=10
    )
    (
    input clk,
    input reset,
    input enable,
    input signed [mul_width-bit_shift-1:0] mul_in,
    input signed [mul_width-bit_shift-1:0] mul_const,
    output reg signed [mul_width-bit_shift-1:0] mul_out,
    output reg out_valid
    );

    initial out_valid=1'b0;

    reg signed [mul_width-1:0] mul_result;
    always @(posedge clk)
    begin
        if(!reset)
        begin
            out_valid <= 1'b0;
        end
        else
        begin
            if(enable)
                mul_result <= (mul_in * mul_const);

            out_valid <= enable;
        end
    end

    always @(*)
        mul_out = mul_result[mul_width-1:bit_shift];

endmodule

