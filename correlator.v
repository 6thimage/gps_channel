module correlator(
    input clk,
    input reset,
    /* baseband */
    input rf_data_valid,
    input [1:0] I, Q,
    /* configured at reset - these should be held constant when reset is high */
    input [4:0] prn,
    /* can be changed at any time */
    input baseband_mode, /* 0 for 2 bit I, Q, 1 for 3 bit I */
    input signed [15:0] carrier_c1_w, carrier_c2_w, carrier_c1_c, carrier_c2_c,
    input signed [25:0] code_c1, code_c2,
    /* active configuration */
    input configure,
    input [29:0] conf_carrier_theta,
    input [29:0] conf_carrier_step,
    input [9:0] conf_code_number,
    input [35:0] conf_code_step,
    /* outputs */
    output running,
    output code_sign,
    output code_sign_valid,
    input code_num_req,
    output reg [45:0] code_num,
    output optimistic_lock,
    output pessimistic_lock
    );

    /**************************************************************** configuration */
    /* The configure inputs starts a configuration cycle, this causes:
     *     * the carrier nco to be reset to conf_carrier_theta
     *     * the code nco to be slewed to conf_code_number
     *     * both the carrier and code steps to be set to their conf_* values
     *     * the bilinear error filters to be reset
     *
     * The configure input should only be pulsed high for one clock cycle,
     * any longer risks the possibility of oscillating the internal
     * configuring state.
     */
    /* carrier nco */
    reg carrier_nco_reset=1'b0;
    /* code nco */
    reg slew=1'b0;
    wire slew_complete;

    reg configuring=1'b0;
    always @(posedge clk)
    begin
        if(!reset)
        begin
            carrier_nco_reset <= 1'b0;

            slew <= 1'b0;

            configuring <= 1'b0;
        end
        else
        begin
            carrier_nco_reset <= 1'b1;

            if(configure || configuring)
                carrier_nco_reset <= 1'b0;

            if(configure)
            begin
                configuring <= 1'b1;
                slew <= 1'b1;
            end

            if(configuring && slew_complete)
            begin
                carrier_nco_reset <= 1'b1;

                configuring <= 1'b0;
                slew <= 1'b0;
            end
        end
    end

    /* running is high unless we are in reset (reset low) or the configure input
     * or configuration cycle are running
     */
    assign running=(reset && !configure && !configuring);

    localparam filter_mul=10;
    localparam code_mul_shift=8;
    /* stage data widths */
    localparam width_RF=4;
    localparam width_carrier_trig=3;         /* sin and cos vary between -3 and 3 -- 3 bits */
    localparam width_carrier=7;              /* varies between -(3*7) and (3*7), mag of 21 -- 6 bits */
    localparam width_despread=width_carrier; /* only a sign change */
    localparam width_integrators=16;         /* amplitude up to 21*16*1023 -- 20 bits */
    localparam width_carrier_error=width_integrators; /* same as integrators, but sign can change */
    localparam width_code_magnitude=width_integrators+2;
    localparam width_code_error=width_code_magnitude+code_mul_shift;

    /**************************************************************** stage zero - baseband decoding */
    localparam BASEBAND_MODE_2BIT=0, BASEBAND_MODE_3BIT=1;

    reg signed [width_RF-1:0] I_rf, Q_rf;
    /* assignment of rf data based on mode */
    always @(*)
    begin
        case(baseband_mode)
        BASEBAND_MODE_2BIT:
        begin
            case(I)
            2'b00: I_rf = 4'd3;
            2'b01: I_rf = 4'd7;
            2'b10: I_rf = -4'd3;
            2'b11: I_rf = -4'd7;
            endcase

            case(Q)
            2'b00: Q_rf = 4'd3;
            2'b01: Q_rf = 4'd7;
            2'b10: Q_rf = -4'd3;
            2'b11: Q_rf = -4'd7;
            endcase
        end

        BASEBAND_MODE_3BIT:
        begin
            /* 3 bit only has I, so use I for both I and Q */
            case({I, Q[1]})
            3'b000: I_rf = 4'd1;
            3'b001: I_rf = 4'd3;
            3'b010: I_rf = 4'd5;
            3'b011: I_rf = 4'd7;
            3'b100: I_rf = -4'd1;
            3'b101: I_rf = -4'd3;
            3'b110: I_rf = -4'd5;
            3'b111: I_rf = -4'd7;
            endcase

            Q_rf = I_rf;
        end
        endcase
    end

    /**************************************************************** first stage - carrier removal */
    wire signed [width_carrier_trig-1:0] carrier_sin, carrier_cos;
    reg signed [29:0] carrier_step;
    wire data_valid_carrier;
    carrier_nco carrier(.clk(clk), .reset(carrier_nco_reset&&reset), .init_theta(conf_carrier_theta),
                        .theta_step(carrier_step), .tick(rf_data_valid),
                        .sin(carrier_sin), .cos(carrier_cos), .sin_cos_valid(data_valid_carrier));

    /* demodulated signal */
    reg signed [width_carrier-1:0] I_carrier, Q_carrier;
    always @(*)
    begin
        I_carrier = I_rf * carrier_sin;
        Q_carrier = Q_rf * carrier_cos;
    end

    /**************************************************************** second stage - code removal */
    reg [35:0] code_step;
    wire code_early, code_prompt, code_late;
    wire [9:0] code_num_coarse;
    wire [35:0] code_num_fine;
    code_nco code(.clk(clk), .reset(reset),
                  .code_step(code_step), .tick(rf_data_valid), .prn(prn),
                  .slew(slew), .slew_code_num(conf_code_number), .slew_complete(slew_complete),
                  .early(code_early), .prompt(code_prompt), .late(code_late),
                  .code_num(code_num_coarse), .code_num_fine(code_num_fine));

    always @(posedge clk)
    begin
        if(code_num_req)
            code_num <= {code_num_coarse, code_num_fine};
    end

    /* convert epl from [0, 1] to [-1, 1] */
    wire signed [1:0] early=code_early?2'd1:-2'd1;
    wire signed [1:0] prompt=code_prompt?2'd1:-2'd1;
    wire signed [1:0] late=code_late?2'd1:-2'd1;

    /* code removal only changes sign */
    reg signed [width_despread-1:0] I_early, I_prompt, I_late, Q_early, Q_prompt, Q_late;
    always @(*)
    begin
        I_early  = I_carrier * early;
        I_prompt = I_carrier * prompt;
        I_late   = I_carrier * late;

        Q_early  = Q_carrier * early;
        Q_prompt = Q_carrier * prompt;
        Q_late   = Q_carrier * late;
    end

    /**************************************************************** third stage - integrate & dump */
    reg signed [width_integrators-1:0] I_early_int='d0, I_prompt_int='d0, I_late_int='d0;
    reg signed [width_integrators-1:0] Q_early_int='d0, Q_prompt_int='d0, Q_late_int='d0;
    reg integrator_dump=1'b0;
    reg signed [width_integrators-1:0] I_early_dump='d0, I_prompt_dump='d0, I_late_dump='d0;
    reg signed [width_integrators-1:0] Q_early_dump='d0, Q_prompt_dump='d0, Q_late_dump='d0;

    reg [13:0] int_counter='d0;

    always @(posedge clk)
    begin
        if(!reset)
        begin
            integrator_dump <= 1'b0;

            I_early_int  <= 'd0;
            I_prompt_int <= 'd0;
            I_late_int   <= 'd0;

            Q_early_int  <= 'd0;
            Q_prompt_int <= 'd0;
            Q_late_int   <= 'd0;

            int_counter <= 'd0;
        end
        else
        begin
            integrator_dump <= 1'b0;

            if(data_valid_carrier)
            begin
                if(int_counter<16368)
                begin
                    I_early_int  <= I_early_int  +
                                    {{(width_integrators-width_despread){I_early[width_despread-1]}}, I_early};
                    I_prompt_int <= I_prompt_int +
                                    {{(width_integrators-width_despread){I_prompt[width_despread-1]}}, I_prompt};
                    I_late_int   <= I_late_int   +
                                    {{(width_integrators-width_despread){I_late[width_despread-1]}}, I_late};

                    Q_early_int  <= Q_early_int  +
                                    {{(width_integrators-width_despread){Q_early[width_despread-1]}}, Q_early};
                    Q_prompt_int <= Q_prompt_int +
                                    {{(width_integrators-width_despread){Q_prompt[width_despread-1]}}, Q_prompt};
                    Q_late_int   <= Q_late_int   +
                                    {{(width_integrators-width_despread){Q_late[width_despread-1]}}, Q_late};

                    int_counter <= int_counter + 1'b1;
                end
                else
                begin
                    integrator_dump <= 1'b1;

                    //$display("%t: integrator dump", $time);
                    /* data output */
                    I_early_dump  <= I_early_int;
                    I_prompt_dump <= I_prompt_int;
                    I_late_dump   <= I_late_int;

                    Q_early_dump  <= Q_early_int;
                    Q_prompt_dump <= Q_prompt_int;
                    Q_late_dump   <= Q_late_int;

                    /* reset integrators */
                    I_early_int  <= {{(width_integrators-width_despread){I_early[width_despread-1]}}, I_early};
                    I_prompt_int <= {{(width_integrators-width_despread){I_prompt[width_despread-1]}}, I_prompt};
                    I_late_int   <= {{(width_integrators-width_despread){I_late[width_despread-1]}}, I_late};

                    Q_early_int  <= {{(width_integrators-width_despread){Q_early[width_despread-1]}}, Q_early};
                    Q_prompt_int <= {{(width_integrators-width_despread){Q_prompt[width_despread-1]}}, Q_prompt};
                    Q_late_int   <= {{(width_integrators-width_despread){Q_late[width_despread-1]}}, Q_late};

                    int_counter <= 'd1;
                end
            end
        end
    end

    /**************************************************************************** lock detector */
    lock_detector #(.prompt_width(width_integrators))
                  lock_detect(.clk(clk), .reset(reset),
                              .prompt_valid(integrator_dump), .prompt_I(I_prompt_dump),
                                  .prompt_Q(Q_prompt_dump),
                              .optimistic_lock(optimistic_lock), .pessimistic_lock(pessimistic_lock));

    /**************************************************************** fourth stage - error calc */
    /* carrier
     * use a decision directed costas
     *     error = sign(I) * Q
     */
    wire signed [width_carrier_error-1:0] carrier_error;
    wire carrier_error_valid;
    //classic_costas #(.width(width_carrier_error)) carrier_costas(
    directed_costas #(.width(width_carrier_error)) carrier_costas(
    //divider_costas #(.width(width_carrier_error)) carrier_costas(
                    .clk(clk), .reset(reset),
                    .I(I_prompt_dump), .Q(Q_prompt_dump), .IQ_valid(integrator_dump),
                    .error(carrier_error), .error_valid(carrier_error_valid));

    assign code_sign_valid=integrator_dump;
    assign code_sign=(I_prompt_dump<0)?1'b1:1'b0;
    /*always @(posedge clk)
        if(code_sign_valid)
            $display("CS %b", code_sign);*/

    wire signed [width_code_error-1:0] code_error;
    wire code_error_valid;
    //non_coherent_power_elgate code_elgate(.clk(clk), .reset(reset),
    non_coherent_envelope_elgate #(.integrator_width(width_integrators), .mul_shift(code_mul_shift),
                                   .magnitude_width(width_code_magnitude), .divider_width(width_code_error))
                                 code_elgate(.clk(clk), .reset(reset),
                                             .I_early(I_early_dump), .I_late(I_late_dump),
                                             .Q_early(Q_early_dump), .Q_late(Q_late_dump),
                                             .IQ_valid(integrator_dump),
                                             .error(code_error), .error_valid(code_error_valid));

    /**************************************************************** fifth stage - filtering */
    wire carrier_filtered_valid;
    wire signed [width_carrier_error-1:0] carrier_filtered_error;

    wire code_filtered_valid;
    wire signed [width_code_error-1:0] code_filtered_error;

    /* carrier constant mux */
    reg signed [15:0] carrier_c1, carrier_c2;

    /* if pessimistic lock, use close loop filters, otherwise use wide */
    always @*
    begin
        if(pessimistic_lock)
        begin
            carrier_c1 = carrier_c1_c;
            carrier_c2 = carrier_c2_c;
        end
        else
        begin
            carrier_c1 = carrier_c1_w;
            carrier_c2 = carrier_c2_w;
        end
    end

    /* as code_error_valid is 36 clocks behind carrier_error_valid
     * we can re-use the same filter for both of the errors
     *
     * e.g. carrier_error_valid    at 66976
     *      carrier_filtered_valid at 66980 +4
     *      code_error_valid       at 67012 +32
     *      code_filtered_valid    at 67017 +5
     */
    filter_simple_combined #(.short_width(width_carrier_error), .long_width(width_code_error),
                             .mul_width(filter_mul))
                            combined_filter(.clk(clk), .reset(reset), .clear(configuring),
                                            .short_constant_c1(carrier_c1),
                                            .short_constant_c2(carrier_c2),
                                            .short_input_valid(carrier_error_valid),
                                            .short_filter_in(carrier_error),
                                            .short_output_valid(carrier_filtered_valid),
                                            .short_filter_out(carrier_filtered_error),
                                            .long_constant_c1(code_c1),
                                            .long_constant_c2(code_c2),
                                            .long_input_valid(code_error_valid),
                                            .long_filter_in(code_error),
                                            .long_output_valid(code_filtered_valid),
                                            .long_filter_out(code_filtered_error));

    /**************************************************************** sixth stage - NCO update */
    always @(posedge clk)
    begin
        if(!reset)
        begin
            carrier_step <= conf_carrier_step;
            code_step <= conf_code_step;
        end
        else
        begin
            if(carrier_filtered_valid)
            begin
                carrier_step <= $unsigned($signed(carrier_step) +
                    $signed({{14{carrier_filtered_error[width_carrier_error-1]}}, carrier_filtered_error}));
                /* synthesis translate_off */
                $display("%t: carrier error %d step %d", $time, carrier_filtered_error, carrier_step);
                /* synthesis translate_on */
            end

            if(code_filtered_valid)
            begin
                code_step <= $unsigned($signed(code_step) +
                    $signed({{10{code_filtered_error[width_code_error-1]}}, (code_filtered_error>>>(code_mul_shift))}));
                /* synthesis translate_off */
                $display("%t: code error %d step %d num %d %d", $time, code_filtered_error>>>(code_mul_shift),
                            code_step, code_num, code_num_fine);
                /* synthesis translate_on */
            end

            if(configuring)
            begin
                carrier_step <= conf_carrier_step;
                code_step <= conf_code_step;
            end
        end
    end

endmodule

