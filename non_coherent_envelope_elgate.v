module non_coherent_envelope_elgate #(
    parameter integrator_width=16,
    parameter mul_shift=10,
    parameter magnitude_width=integrator_width+2,
    parameter divider_width=magnitude_width+mul_shift
    )
    (
    input clk,
    input reset,
    input signed [integrator_width-1:0] I_early, I_late, Q_early, Q_late,
    input IQ_valid,
    output signed [divider_width-1:0] error,
    output error_valid
    );

    /* code
     * non-coherent, normalised early-late gate
     *     (early - late) / 2.*(early + late)
     * where early and late are the magnitudes of the complex correlators
     * i.e. sqrt(I_e^2 + Q_e^2) and sqrt(I_l^ + Q_l^2)
     */

    /* magnitude estimation */
    reg  mag_est_enable=1'b0;
    reg signed [integrator_width-1:0] mag_est_I, mag_est_Q;
    wire [magnitude_width-1:0] mag_est_output;
    wire mag_est_valid;
    magnitude_estimator #(.width(integrator_width)) mag_est(.clk(clk), .enable(mag_est_enable),
                                .I(mag_est_I), .Q(mag_est_Q),
                                .magnitude(mag_est_output), .output_valid(mag_est_valid));

    reg [integrator_width+1:0] code_mag_early, code_mag_late;
    reg code_mag_valid=1'b0;

    localparam MAG_STATE_IDLE=0, MAG_STATE_EARLY=1, MAG_STATE_LATE=2, MAG_STATE_END=3;
    reg [1:0] mag_state=MAG_STATE_IDLE;
    //assign mag_est_enable=(mag_state!=MAG_STATE_IDLE);
    always @(posedge clk)
    begin
        if(!reset)
        begin
            mag_state <= MAG_STATE_IDLE;

            mag_est_enable <= 1'b0;

            code_mag_valid <= 1'b0;
        end
        else
        begin
            code_mag_valid <= 1'b0;

            mag_est_enable <= 1'b0;

            case(mag_state)
            MAG_STATE_IDLE:
            begin
                if(IQ_valid)
                    mag_state <= MAG_STATE_EARLY;
            end

            MAG_STATE_EARLY:
            begin
                //$display("CE %d %d", I_early, Q_early);
                //$display("CL %d %d", I_late, Q_late);
                mag_est_I <= I_early;
                mag_est_Q <= Q_early;

                mag_est_enable <= 1'b1;

                mag_state <= MAG_STATE_LATE;
            end

            MAG_STATE_LATE:
            begin
                if(mag_est_valid)
                begin
                    //$display("mag early %d", mag_est_output);
                    code_mag_early <= mag_est_output;

                    mag_est_I <= I_late;
                    mag_est_Q <= Q_late;

                    mag_est_enable <= 1'b1;

                    mag_state <= MAG_STATE_END;
                end
            end

            MAG_STATE_END:
            begin
                if(mag_est_valid)
                begin
                    //$display("mag late %d", mag_est_output);
                    code_mag_late <= mag_est_output;

                    code_mag_valid <= 1'b1;

                    mag_state <= MAG_STATE_IDLE;
                end
            end
            endcase

        end
    end

    /* difference & division */
    reg signed [divider_width-1:0] code_diff_neg, code_diff_pos;

    reg code_div_enable=1'b0;
    signed_divider #(.width(divider_width), .rounding(1)) code_div(
                                          .clk(clk), .reset(reset), .enable(code_div_enable),
                                          .dividend(code_diff_neg), .divisor(code_diff_pos),
                                          .quotient(error), /*.remainder(),*/
                                          .output_valid(error_valid)/*, .idle()*/);

    localparam DIFF_STATE_IDLE=0, DIFF_STATE_NEG=1, DIFF_STATE_POS=2, DIFF_STATE_DIV=3;
    reg [1:0] diff_state=DIFF_STATE_IDLE;

    localparam div_neg_pad=divider_width-integrator_width-2;

    always @(posedge clk)
    begin
        if(!reset)
        begin
            diff_state <= DIFF_STATE_IDLE;

            code_div_enable <= 1'b0;
        end
        else
        begin
            code_div_enable <= 1'b0;

            case(diff_state)
            DIFF_STATE_IDLE:
            begin
                if(code_mag_valid)
                    diff_state <= DIFF_STATE_NEG;
            end

            DIFF_STATE_NEG:
            begin
                code_diff_neg <= ($signed({{div_neg_pad{code_mag_early[integrator_width]}}, code_mag_early})
                                  - $signed({{div_neg_pad{code_mag_late[integrator_width]}},code_mag_late})
                                 )<<<mul_shift;

                diff_state <= DIFF_STATE_POS;
            end

            DIFF_STATE_POS:
            begin
                code_diff_pos <= ($signed({{div_neg_pad{code_mag_early[integrator_width]}}, code_mag_early})
                                  + $signed({{div_neg_pad{code_mag_late[integrator_width]}},code_mag_late})
                                 )<<<1;

                diff_state <= DIFF_STATE_DIV;
            end

            DIFF_STATE_DIV:
            begin
                //$display("D %d %d", code_diff_neg, code_diff_pos);
                code_div_enable <= 1'b1;

                diff_state <= DIFF_STATE_IDLE;
            end
            endcase
        end
    end

endmodule

