module signed_divider #(
    parameter width=16
    )
    (
    input clk,
    input reset,
    input enable,
    input signed [width-1:0] dividend,
    input signed [width-1:0] divisor,
    output reg signed [width-1:0] quotient,
    output reg signed [width-1:0] remainder,
    output reg output_valid,
    output idle
    );

    initial output_valid=1'b0;

    /* sign removal */
    reg [width-1:0] dividend_unsigned, divisor_unsigned;
    always @(*)
    begin
        if(dividend<0)
            dividend_unsigned = -$unsigned(dividend);
        else
            dividend_unsigned = $unsigned(dividend);

        if(divisor<0)
            divisor_unsigned = -$unsigned(divisor);
        else
            divisor_unsigned = $unsigned(divisor);
    end

    /* divison register */
    reg [(2*width)-1:0] qr;

    /* signed output */
    reg quotient_sign, remainder_sign;
    always @(*)
    begin
        if(remainder_sign)
            remainder = -$signed(qr[(2*width)-1:width]);
        else
            remainder = $signed(qr[(2*width)-1:width]);

        if(quotient_sign)
            quotient = -$signed(qr[width-1:0]);
        else
            quotient = $signed(qr[width-1:0]);
    end

    /* bit counter */
    reg [log2(width)-1:0] bit_counter='d0;

    /* subtractor */
    reg signed [width:0] difference;
    always @(*)
        difference = $signed(qr[(2*width)-1:width-1]) - $signed(divisor_unsigned);

    /* state machine */
    localparam STATE_IDLE=0, STATE_DIVIDING=1;
    reg state=STATE_IDLE;
    assign idle=(state==STATE_IDLE)?1'b1:1'b0;

    always @(posedge clk)
    begin
        if(!reset)
        begin
            state <= STATE_IDLE;

            output_valid <= 1'b0;

            bit_counter <= 'd0;
        end
        else
        begin
            output_valid <= 1'b0;

            case(state)
            STATE_IDLE:
            begin
                if(enable)
                begin
                    state <= STATE_DIVIDING;

                    /* sign detection */
                    case({dividend<0, divisor<0})
                    2'b00: {quotient_sign, remainder_sign} <= 2'b00;
                    2'b01: {quotient_sign, remainder_sign} <= 2'b10;
                    2'b10: {quotient_sign, remainder_sign} <= 2'b11;
                    2'b11: {quotient_sign, remainder_sign} <= 2'b01;
                    endcase

                    qr <= {{width{1'b0}}, dividend_unsigned};

                    bit_counter <= 'd0;
                end
            end

            STATE_DIVIDING:
            begin
                if(difference<0)
                    qr <= qr<<1;
                else
                    qr <= {$unsigned(difference[width-1:0]), qr[width-2:0], 1'b1};

                if(bit_counter < (width-1))
                    bit_counter <= bit_counter + 1'b1;
                else
                begin
                    state <= STATE_IDLE;
                    output_valid <= 1'b1;
                end
            end
            endcase
        end
    end

    /* synthesis function to generate width required for value */
    function integer log2(input integer n);
        integer i;
        begin
            log2=1;
            for(i=0; 2**i<n; i=i+1)
                log2=i+1;
        end
    endfunction

endmodule
