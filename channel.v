module channel(
    input clk,
    input reset,
    /* baseband */
    input rf_data_valid,
    input [1:0] I, Q,
    /* correlator */
    input [4:0] prn,
    input baseband_mode,
    input signed [15:0] carrier_c1_w, carrier_c2_w, carrier_c1_c, carrier_c2_c,
    input signed [25:0] code_c1, code_c2,
    input configure,
    input [29:0] conf_carrier_theta,
    input [29:0] conf_carrier_step,
    input [9:0] conf_code_number,
    input [35:0] conf_code_step,
    output correlator_running,
    input code_num_req,
    output [45:0] code_num,
    output optimistic_lock,
    output pessimistic_lock,
    /* decoder */
    output frame_ready,
    output [2:0] frame_valid,
    output [16:0] TOW,
    output alert_flag,
    output enhanced_integrity,
    /* */
    output [9:0] week_number,
    output [3:0] URA_index,
    output SV_health_bad,
    output [4:0] SV_health,
    output [9:0] IODC,
    output [15:0] t_oc,
    output signed [7:0] T_GD,
    output signed [7:0] a_f2,
    output signed [15:0] a_f1,
    output signed [21:0] a_f0,
    /* */
    output [7:0] IODE_sf2,
    output curve_fit_greater,
    output signed [31:0] M_0,
    output signed [31:0] ecc,
    output [31:0] sqrt_A,
    output signed [15:0] Delta_n,
    output signed [15:0] t_oe,
    output signed [15:0] C_rs,
    output signed [15:0] C_uc,
    output signed [15:0] C_us,
    /* */
    output [7:0] IODE_sf3,
    output signed [31:0] Omega_0,
    output signed [31:0] incl_0,
    output signed [31:0] omega,
    output signed [23:0] Omega_dot,
    output signed [15:0] C_rc,
    output signed [15:0] C_ic,
    output signed [15:0] C_is,
    output signed [13:0] IDOT
    );


    /* correlator */
    wire code_sign, code_sign_valid;
    correlator ch_correlator(.clk(clk), .reset(reset),
                             .rf_data_valid(rf_data_valid), .I(I), .Q(Q),
                             .prn(prn), .baseband_mode(baseband_mode), .carrier_c1(carrier_c1),
                                 .carrier_c2(carrier_c2), .code_c1(code_c1), .code_c2(code_c2),
                             .configure(configure), .conf_carrier_theta(conf_carrier_theta),
                                 .conf_carrier_step(conf_carrier_step),
                                 .conf_code_number(conf_code_number), .conf_code_step(conf_code_step),
                             .running(correlator_running), .code_sign(code_sign),
                                 .code_sign_valid(code_sign_valid), .code_num_req(code_num_req),
                                 .code_num(code_num), .optimistic_lock(optimistic_lock),
                                 .pessimistic_lock(pessimistic_lock));

    /* decoder */
    decoder ch_decoder(.clk(clk), .reset(reset),
                       .code_sign_pos(code_sign), .code_sign_valid(code_sign_valid),
                       .frame_ready(frame_ready), .frame_valid(frame_valid), .preamble_tick(),
                       .TOW(TOW), .alert_flag(alert_flag), .enhanced_integrity(enhanced_integrity),
                       .week_number(week_number), .URA_index(URA_index), .SV_health_bad(SV_health_bad),
                           .SV_health(SV_health), .IODC(IODC), .t_oc(t_oc), .T_GD(T_GD), .a_f2(a_f2),
                           .a_f1(a_f1), .a_f0(a_f0),
                       .IODE_sf2(IODE_sf2), .curve_fit_greater(curve_fit_greater), .M_0(M_0),
                           .ecc(ecc), .sqrt_A(sqrt_A), .Delta_n(Delta_n), .t_oe(t_oe), .C_rs(C_rs),
                           .C_uc(C_uc),.C_us(C_us),
                       .IODE_sf3(IODE_sf3), .Omega_0(Omega_0), .incl_0(incl_0), .omega(omega),
                           .Omega_dot(Omega_dot), .C_rc(C_rc), .C_ic(C_ic), .C_is(C_is), .IDOT(IDOT));

endmodule
