module filter_simple_combined #(
    parameter short_width=10,
    parameter long_width=10,
    parameter mul_width=10
    )
    (
    input clk,
    input reset,
    input clear,
    /* short filter */
    /* filter config */
    input signed [short_width-1:0] short_constant_c1, short_constant_c2,
    /* filter inputs */
    input short_input_valid,
    input signed [short_width-1:0] short_filter_in,
    /* filter outputs */
    output reg short_output_valid,
    output reg signed [short_width-1:0] short_filter_out,
    /* long filter */
    /* filter config */
    input signed [long_width-1:0] long_constant_c1, long_constant_c2,
    /* filter inputs */
    input long_input_valid,
    input signed [long_width-1:0] long_filter_in,
    /* filter outputs */
    output reg long_output_valid,
    output reg signed [long_width-1:0] long_filter_out
    );

    initial
    begin
        short_output_valid=1'b0;
        long_output_valid=1'b0;
    end

    reg mul_enable=1'b0, mul_short=1'b0;
    reg signed [short_width-1:0] short_in, short_const;
    wire signed [short_width-1:0] short_out;
    wire short_out_valid;
    reg signed [long_width-1:0] long_in, long_const;
    wire signed [long_width-1:0] long_out;
    wire long_out_valid;
    constant_mul_combined #(.short_width(short_width+mul_width), .long_width(long_width+mul_width),
                            .bit_shift(mul_width))
                        mul(.clk(clk), .reset(reset), .enable(mul_enable), .short(mul_short),
                            .short_in(short_in), .short_const(short_const), .short_out(short_out),
                                .short_out_valid(short_out_valid),
                            .long_in(long_in), .long_const(long_const), .long_out(long_out),
                                .long_out_valid(long_out_valid));

    /* algorithm:
     *
     * filter_out = c1*filter_in + c2*last_in
     * last_in = filter_in
     *
     * stages
     *
     * -- stage 0
     * c1*filter_in
     *
     * -- stage 1
     * filter_out = [c1*filter_in]
     * c2*last_in
     *
     * -- stage 2
     * filter_out += [c2*last_in]
     * last_in = filter_in
     */
    reg signed [short_width-1:0] short_constant_c2_int;

    reg signed [short_width-1:0] short_last_in='d0;
    reg signed [long_width-1:0] long_last_in='d0;

    reg load=1'b0, unload=1'b0;
    always @(posedge clk)
    begin
        if(!reset)
        begin
            short_output_valid <= 1'b0;
            long_output_valid <= 1'b0;

            load <= 1'b0;
            unload <= 1'b0;
        end
        else
        begin
            short_output_valid <= 1'b0;
            long_output_valid <= 1'b0;

            mul_enable <= 1'b0;

            if(short_input_valid)
            begin
                short_in <= short_filter_in;
                short_const <= short_constant_c1;
                short_constant_c2_int <= short_constant_c2;
                mul_short <= 1'b1;
                mul_enable <= 1'b1;

                unload <= 1'b0;
            end
            else if(long_input_valid)
            begin
                long_in <= long_filter_in;
                long_const <= long_constant_c1;
                mul_short <= 1'b0;
                mul_enable <= 1'b1;

                unload <= 1'b0;
            end

            load <= short_input_valid | long_input_valid;

            if(load)
            begin
                if(mul_short)
                begin
                    short_in <= short_last_in;
                    short_const <= short_constant_c2_int;
                    short_last_in <= short_filter_in;
                end
                else
                begin
                    long_in <= long_last_in;
                    long_const <= long_constant_c2;
                    long_last_in <= long_filter_in;
                end

                mul_enable <= 1'b1;
            end

            if(short_out_valid)
            begin
                unload <= ~unload;
                if(!unload)
                    short_filter_out <= short_out;
                else
                begin
                    short_filter_out <= short_filter_out + short_out;
                    short_output_valid <= 1'b1;
                end

            end
            else if(long_out_valid)
            begin
                unload <= ~unload;
                if(!unload)
                    long_filter_out <= long_out;
                else
                begin
                    long_filter_out <= long_filter_out + long_out;
                    long_output_valid <= 1'b1;
                end
            end

            if(clear)
            begin
                short_last_in <= 'd0;
                long_last_in <= 'd0;
            end
        end
    end

endmodule

