module sin_cos_table_tb();
    /* inputs */
    reg [3:0] theta;
    /* outputs */
    wire signed [2:0] cos, sin;

    sin_cos_table uut(.theta(theta), .sin(sin), .cos(cos));

    integer i;

    initial
    begin
        for(i=0; i<16; i=i+1)
        begin
            theta=i;
            #1 $display("theta: %2d sin %2d cos %2d", theta, sin, cos);
        end
        $finish;
    end

endmodule

