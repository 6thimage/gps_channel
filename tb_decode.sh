#! /bin/bash

set -e

verilator -cc decoder.v --exe verilator_decoder.cpp

pushd obj_dir
make -j -f Vdecoder.mk Vdecoder
popd

if [ -f obj_dir/Vdecoder ]; then
    obj_dir/Vdecoder
fi

rm -r obj_dir
