module directed_costas #(
    parameter width=20
    )
    (
    input clk,
    input reset,
    input signed [width-1:0] I, Q,
    input IQ_valid,
    output reg signed [width-1:0] error,
    output reg error_valid
    );

    initial error_valid=1'b0;

    always @(*)
    begin
        error = Q;
        if(I<0)
            error = -error;
    end

    always @(posedge clk)
    begin
        if(!reset)
            error_valid <= 1'b0;
        else
            error_valid <= IQ_valid;
    end

endmodule
