module signed_divider #(
    parameter width=16,
    parameter rounding=0
    )
    (
    input clk,
    input reset,
    input enable,
    input signed [width-1:0] dividend,
    input signed [width-1:0] divisor,
    output reg signed [width-1:0] quotient,
    output reg output_valid
    );

    initial output_valid=1'b0;

    /* sign removal */
    reg [width-1:0] dividend_unsigned, divisor_unsigned;
    always @(*)
    begin
        if(dividend<0)
            dividend_unsigned = $unsigned(-dividend);
        else
            dividend_unsigned = $unsigned(dividend);

        if(divisor<0)
            divisor_unsigned = $unsigned(-divisor);
        else
            divisor_unsigned = $unsigned(divisor);
    end

    /* divison register */
    reg [(2*width)-1:0] qr;

    /* rounding */
    wire rounding_bit=(rounding)?((qr[(2*width)-1:width]>=divisor_unsigned)?1'b1:1'b0):1'b0;

    /* signed output */
    wire [width-1:0] quotient_unsigned=qr[width-1:0] + {{(width-1){1'b0}}, rounding_bit};
    reg quotient_sign;
    always @(*)
    begin
        if(quotient_sign)
            quotient = $signed(-quotient_unsigned);
        else
            quotient = $signed(quotient_unsigned);
    end

    /* bit counter */
    reg [log2(width)-1:0] bit_counter='d0;

    /* subtractor */
    reg signed [width:0] difference;
    always @(*)
        difference = $signed(qr[(2*width)-1:width-1]) - $signed(divisor_unsigned);

    /* state machine */
    localparam STATE_IDLE=0, STATE_DIVIDING=1;
    reg state=STATE_IDLE;

    always @(posedge clk)
    begin
        if(!reset)
        begin
            state <= STATE_IDLE;

            output_valid <= 1'b0;

            bit_counter <= 'd0;
        end
        else
        begin
            output_valid <= 1'b0;

            case(state)
            STATE_IDLE:
            begin
                if(enable)
                begin
                    state <= STATE_DIVIDING;

                    /* sign detection */
                    case({dividend<0, divisor<0})
                    2'b00: quotient_sign <= 1'b0;
                    2'b01: quotient_sign <= 1'b1;
                    2'b10: quotient_sign <= 1'b1;
                    2'b11: quotient_sign <= 1'b0;
                    endcase

                    qr <= {{width{1'b0}}, dividend_unsigned};

                    bit_counter <= 'd0;
                end
            end

            STATE_DIVIDING:
            begin
                if(difference<0)
                    qr <= qr<<1;
                else
                    qr <= {$unsigned(difference[width-1:0]), qr[width-2:0], 1'b1};

                /* verilator lint_off WIDTH */
                if(bit_counter < (width-1))
                /* verilator lint_on WIDTH */
                    bit_counter <= bit_counter + 1'b1;
                else
                begin
                    state <= STATE_IDLE;
                    output_valid <= 1'b1;
                end
            end
            endcase
        end
    end

    /* synthesis function to generate width required for value */
    function integer log2(input integer n);
        integer i;
        begin
            log2=1;
            for(i=0; 2**i<n; i=i+1)
                log2=i+1;
        end
    endfunction

endmodule
