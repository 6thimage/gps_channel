module decoder(
    input clk,
    input reset,
    /* from correlator */
    input code_sign_pos,
    input code_sign_valid,
    /* outputs */
    output reg frame_ready,        /* pulses high for one clock cycle when there is valid output data */
    output reg [2:0] frame_valid,  /* indicates which subframes are valid
                                      bit i represents subframe i+1 */
    output reg preamble_tick,      /* pulses high for one clock cycle when a valid (parity-checked)
                                      preamble is detected */
    /* all frame outputs */
    output reg [16:0] TOW,         /* */
    output reg alert_flag,         /* URA may be worse than in subframe 1 */
    output reg enhanced_integrity, /* if set, the probability of exceeding the alert limit,
                                      without triggering the alert flag is lower */
        /* L2 P or C/A - not needed as we don't care about L2 */
        /* anti-spoof flag - not needed as L2 only*/
    /* subframe 1 outputs */
    output reg [9:0] week_number, /* */
    output reg [3:0] URA_index,   /* SV accuracy */
    output reg SV_health_bad,     /* if set, some of the NAV data is bad */
    output reg [4:0] SV_health,   /* indicates signal health problem */
    output reg [9:0] IODC,        /* issue of data - clock */
    output reg [15:0] t_oc,       /* clock data reference time */
        /* L2 P code nav stream */
    output reg signed [7:0] T_GD, /* signal group delay */
    output reg signed [7:0] a_f2,  /* clock correction parameter */
    output reg signed [15:0] a_f1, /* clock correction parameter */
    output reg signed [21:0] a_f0, /* clock correction parameter */
    /* subframe 2 outputs */
    output reg [7:0] IODE_sf2,          /* issue of data - ephemeris */
    output reg curve_fit_greater,       /* if set, the curve fit is greater than 4 hours */
    output reg signed [31:0] M_0,       /* mean anomaly */
    output reg signed [31:0] ecc,       /* eccentricity */
    output reg [31:0] sqrt_A,           /* sqaure root of semi-major axis */
    output reg signed [15:0] Delta_n,   /* mean motion difference */
    output reg signed [15:0] t_oe,      /* reference time */
    output reg signed [15:0] C_rs,      /* orbit radius correction - sine */
    output reg signed [15:0] C_uc,      /* argument of lattitude correction - cosine */
    output reg signed [15:0] C_us,      /* argument of lattitude correction - sine */
    /* subframe 3 outputs */
    output reg [7:0] IODE_sf3,          /* issue of data - ephemeris */
    output reg signed [31:0] Omega_0,   /* longitude of ascending node */
    output reg signed [31:0] incl_0,    /* inclination */
    output reg signed [31:0] omega,     /* RAAN */
    output reg signed [23:0] Omega_dot, /* rate of right ascension */
    output reg signed [15:0] C_rc,      /* orbit radius correction - cosine */
    output reg signed [15:0] C_ic,      /* inclination correction - cosine */
    output reg signed [15:0] C_is,      /* inclination correction - sine */
    output reg signed [13:0] IDOT       /* rate of inclination */
    );

    initial
    begin
        frame_ready=1'b0;
        frame_valid='d0;
        preamble_tick=1'b0;
    end

    reg [4:0] decode_counter='d0;
    reg [4:0] code_pos_counter='d0;
    reg bit_sign=1'b0, bit_sign_valid=1'b0;

    reg last_sign=1'b0;
    reg multiple_sign_changes=1'b0, sign_changed=1'b0, sign_low=1'b0, sign_high=1'b0;

    always @(posedge clk)
    begin
        if(!reset)
        begin
            decode_counter <= 'd0;
            code_pos_counter <= 'd0;

            bit_sign <= 1'b0;
            bit_sign_valid <= 1'b0;

            last_sign <= 1'b0;
            multiple_sign_changes <= 1'b0;
            sign_changed <= 1'b0;
            sign_low <= 1'b0;
            sign_high <= 1'b0;
        end
        else
        begin
            bit_sign_valid <= 1'b0;

            if(code_sign_valid)
            begin
                /* increment counter */
                decode_counter <= decode_counter + 1'b1;

                /* add sign to counter */
                code_pos_counter <= code_pos_counter + {4'd0, code_sign_pos};

                /* sign change detection */
                if(code_sign_pos!=last_sign)
                begin
                    /* store current sign & mark that a change has occurred */
                    last_sign <= code_sign_pos;
                    sign_changed <= 1'b1;

                    /* mark if there have been multiple sign changes */
                    if(sign_changed)
                        multiple_sign_changes <= 1'b1;

                    /* set high and low markers */
                    sign_high <= 1'b0;
                    sign_low <= 1'b0;
                    if((decode_counter>'d1) && (decode_counter<'d19))
                    begin
                        if(decode_counter>9)
                            sign_high <= 1'b1;
                        else
                            sign_low <= 1'b1;
                    end
                end

                /* the decode_counter always goes to 19 before it is reset - i.e. so that
                 * 20 sub-bits are read per data bit, however, we want to align the bit
                 * change, so the counter can be reset to 1 or -1 (well, 31) so that we
                 * can shift the bit change to the best postion
                 *
                 * if there are multiple sign changes in a bit period, we, of course, do
                 * not try to adjust the alignment and instead reset to zero, in the hope
                 * that the multiple changes are due to issues with the signal tracking
                 */
                if(decode_counter=='d19)
                begin
                    /* bit output */
                    bit_sign <= (code_pos_counter>10)?1'b1:1'b0;
                    bit_sign_valid <= 1'b1;

                    /* counter reset */
                    decode_counter <= 'd0;
                    if(!multiple_sign_changes)
                    begin
                        if(sign_high)
                            decode_counter <= 'd1;
                        else if(sign_low)
                            decode_counter <= {5{1'b1}};
                    end

                    /* reset code sign counter */
                    code_pos_counter <= 'd0;

                    /* marker resets */
                    sign_changed <= 1'b0;
                    multiple_sign_changes <= 1'b0;
                end
            end
        end
    end

    /* shift bit into word buffer */
    reg [29:0] word='d0;
    reg word_updated=1'b0;
    always @(posedge clk)
    begin
        if(!reset)
        begin
            word <= 'd0;
            word_updated <= 1'b0;
        end
        else
        begin
            word_updated <= 1'b0;

            /* each time we shift a bit in, mark the word as being updated */
            if(bit_sign_valid)
            begin
                word <= {word[28:0], bit_sign};
                word_updated <= 1'b1;
            end
        end
    end

    /* parity calculation */
    /* iword     - output of word inversion (due to preamble polarity)
     * cword     - output of parity complementing
     * dword     - data word, this is used to decode the subframe data, as cword will change
     *             during the decoding as we store the last 2 bits (in dword_low) so we can
     *             correctly check the parity of the next word
     * dword_low - used to store the 2 LSb of the parity, which are used as part of the
     *             parity alogrithm for the next word
     * preamble  - taken directly from collected word
     * parity    - taken from the inverted word to remove receiver polarity
     */
    reg [29:0] iword='d0, cword='d0;
    reg word_invert=1'b0;
    wire [7:0] preamble=word[29:22];
    wire [5:0] parity=iword[5:0];

    reg [5:0] calculated_parity='d0;
    reg [23:0] dword='d0;
    reg [1:0] dword_low=2'b00;

    always @(*)
    begin
        /* invert word if required */
        if(word_invert)
            iword = ~word;
        else
            iword = word;

        /* complement everything but parity, if needed */
        if(dword_low[0])
            cword = {~iword[29:6], iword[5:0]};
        else
            cword = iword;

        /* parity checking as in table 20-XIV, IS-GPS-200H
         * bits remapped from 1-30 in GPS documentation, to 29-0 (preamble 29-22, parity 5-0)
         * i.e. numbers here are 30-(bit number in 20-XIV)
         */
        calculated_parity[5]=dword_low[1]^cword[29]^cword[28]^cword[27]^cword[25]^cword[24]^cword[20]
                                         ^cword[19]^cword[18]^cword[17]^cword[16]^cword[13]^cword[12]
                                         ^cword[10]^cword[7];

        calculated_parity[4]=dword_low[0]^cword[28]^cword[27]^cword[26]^cword[24]^cword[23]^cword[19]
                                         ^cword[18]^cword[17]^cword[16]^cword[15]^cword[12]^cword[11]
                                         ^cword[9] ^cword[6];

        calculated_parity[3]=dword_low[1]^cword[29]^cword[27]^cword[26]^cword[25]^cword[23]^cword[22]
                                         ^cword[18]^cword[17]^cword[16]^cword[15]^cword[14]^cword[11]
                                         ^cword[10]^cword[8];

        calculated_parity[2]=dword_low[0]^cword[28]^cword[26]^cword[25]^cword[24]^cword[22]^cword[21]
                                         ^cword[17]^cword[16]^cword[15]^cword[14]^cword[13]^cword[10]
                                         ^cword[9] ^cword[7];

        calculated_parity[1]=dword_low[0]^cword[29]^cword[27]^cword[25]^cword[24]^cword[23]^cword[21]
                                         ^cword[20]^cword[16]^cword[15]^cword[14]^cword[13]^cword[12]
                                         ^cword[9] ^cword[8] ^cword[6];

        calculated_parity[0]=dword_low[1]^cword[27]^cword[25]^cword[24]^cword[22]^cword[21]^cword[20]
                                         ^cword[19]^cword[17]^cword[15]^cword[11]^cword[8] ^cword[7]
                                         ^cword[6];
    end

    /* decoding_frame - when low, this block will search for a preamble, when high, it counts the
     *                  number of read bits and breaks them into words, returning to preamble
     *                  searching after a frame has been read
     * decode_word    - pulses high for a clock cycle when a frame is ready to decode
     * check_parity   - used to delay the parity checking of the preamble by a clock cycle, to
     *                  allow the parity calculation algorithm to adjust for the polarity of the
     *                  detected preamble (it seems a bit hack-y, but it avoids having to have
     *                  two parity checkers or pipelining this block)
     */
    reg decoding_frame=1'b0;
    reg decode_word=1'b0;
    reg check_parity=1'b0;
    reg [3:0] word_number='d0;
    reg [4:0] bit_counter='d0;
    always @(posedge clk)
    begin
        if(!reset)
        begin
            decoding_frame <= 1'b0;
            decode_word <= 1'b0;
            check_parity <= 1'b0;

            preamble_tick <= 1'b0;

            word_number <= 'd0;
            bit_counter <= 'd0;

            dword <= 'd0;
            dword_low <= 2'b00;
        end
        else
        begin
            decode_word <= 1'b0;
            check_parity <= 1'b0;
            preamble_tick <= 1'b0;

            if(check_parity)
            begin
                if(parity==calculated_parity)
                begin
                    preamble_tick <= 1'b1;

                    decoding_frame <= 1'b1;
                    decode_word <= 1'b1;

                    dword <= cword[29:6];
                    dword_low <= cword[1:0];

                    word_number <= 'd0;
                    bit_counter <= 'd0;

                    /* synthesis translate_off */
                    $display("### good parity");
                    /* synthesis translate_on */
                end
                /* synthesis translate_off */
                else
                    $display("### bad parity");
                /* synthesis translate_on */
            end

            if(word_updated)
            begin
                if(!decoding_frame)
                begin
                    /* preamble detection */
                    if(preamble==8'b10001011)
                    begin
                        word_invert <= 1'b0;
                        check_parity <= 1'b1;
                        /* synthesis translate_off */
                        $display("### positive preamble");
                        /* synthesis translate_on */
                    end
                    else if(preamble==8'b01110100)
                    begin
                        word_invert <= 1'b1;
                        check_parity <= 1'b1;
                        /* synthesis translate_off */
                        $display("### negative preamble");
                        /* synthesis translate_on */
                    end
                end
                else
                begin
                    bit_counter <= bit_counter + 1'b1;

                    if(bit_counter=='d29)
                    begin
                        bit_counter <= 'd0;

                        dword <= cword[29:6];
                        dword_low <= cword[1:0];
                        decode_word <= 1'b1;

                        word_number <= word_number + 1'b1;

                        /* end condition - on either a bad parity or the last word of a frame */
                        if(word_number=='d8 || (parity!=calculated_parity))
                        begin
                            /* synthesis translate_off */
                            $display("### decoder finishing at %d", word_number);
                            /* synthesis translate_on */
                            decoding_frame <= 1'b0;
                            dword_low[1:0] <= 'd0;
                        end
                    end
                end
            end
        end
    end

    /* these 6 wires are used to convert twos complement data in the dword to the correct
     * signed representation (actually, these lines don't do anything but tell the synthesis /
     * simulator the sign)
     */
    wire signed [15:0] dword_29_14=dword[23:8];
    wire signed [15:0] dword_21_6=dword[15:0];
    wire signed [23:0] dword_Omega_dot=dword[23:0];
    wire signed [21:0] dword_a_f0=dword[23:2];
    wire signed [13:0] dword_IDOT=dword[15:2];
    wire signed [7:0] dword_a_f2=dword[23:16];

    /* word by word, frame decoder */
    reg [2:0] subframe_number='d0;
    always @(posedge clk)
    begin
        if(!reset)
        begin
            frame_ready <= 1'b0;
            frame_valid <= 'd0;
            IODC <= 10'h3ff;
            IODE_sf2 <= 8'haa;
            IODE_sf3 <= 8'h55;
        end
        else
        begin
            frame_ready <= 1'b0;

            if(decode_word)
            begin
                case(word_number)
                0:
                begin
                    enhanced_integrity <= dword[1];
                    frame_valid        <= 'd0;
                end
                1:
                begin
                    TOW        <= dword[23:7];
                    alert_flag <= dword[6];

                    /* synthesis translate_off */
                    case(dword[13:12])
                    2'b00: $display("    ### L2: reserved");
                    2'b01: $display("    ### L2: P code on");
                    2'b10: $display("    ### L2: C/A code on");
                    2'b11: $display("    ### L2: alt. bits");
                    endcase
                    if(dword[5])
                        $display("    ### anti-spoof flag set");
                    $display("    ### subframe number: %d", dword[4:2]);
                    /* synthesis translate_on */

                    subframe_number <= dword[4:2];
                end
                2:
                begin
                    case(subframe_number)
                    1:
                    begin
                        week_number   <= dword[23:14];
                        URA_index     <= dword[11:8];
                        SV_health_bad <= dword[7];
                        SV_health     <= dword[6:2];
                        IODC[9:8]     <= dword[1:0];
                    end
                    2:
                    begin
                        IODE_sf2 <= dword[23:16];
                        C_rs     <= dword_21_6;
                    end
                    3:
                    begin
                        C_ic           <= dword_29_14;
                        Omega_0[31:24] <= dword[7:0];
                    end
                    endcase
                end
                3:
                begin
                    case(subframe_number)
                    /* synthesis translate_off */
                    1: $display("    ### L2 P code NAV stream: %s", dword[23]?"off":"on");
                    /* synthesis translate_on */
                    2:
                    begin
                        Delta_n    <= dword_29_14;
                        M_0[31:24] <= dword[7:0];
                    end
                    3: Omega_0[23:0] <= dword[23:0];
                    endcase
                end
                4:
                begin
                    case(subframe_number)
                    2: M_0[23:0] <= dword[23:0];
                    3:
                    begin
                        C_is          <= dword_29_14;
                        incl_0[31:24] <= dword[7:0];
                    end
                    endcase
                end
                5:
                begin
                    case(subframe_number)
                    2:
                    begin
                        C_uc       <= dword_29_14;
                        ecc[31:24] <= dword[7:0];
                    end
                    3: incl_0[23:0] <= dword[23:0];
                    endcase
                end
                6:
                begin
                    case(subframe_number)
                    1: T_GD <= dword[23:16];
                    2: ecc[23:0] <= dword[23:0];
                    3:
                    begin
                        C_rc         <= dword_29_14;
                        omega[31:24] <= dword[7:0];
                    end
                    endcase
                end
                7:
                begin
                    case(subframe_number)
                    1:
                    begin
                        IODC[7:0] <= dword[23:16];
                        t_oc      <= dword[15:0];
                    end
                    2:
                    begin
                        C_us          <= dword_29_14;
                        sqrt_A[31:24] <= dword[7:0];
                    end
                    3: omega[23:0] <= dword[23:0];
                    endcase
                end
                8:
                begin
                    case(subframe_number)
                    1:
                    begin
                        a_f2 <= dword_a_f2;
                        a_f1 <= dword_21_6;
                    end
                    2: sqrt_A[23:0] <= dword[23:0];
                    3: Omega_dot    <= dword_Omega_dot;
                    endcase
                end
                9:
                begin
                    frame_ready <= 1'b1;
                    case(subframe_number)
                    1:
                    begin
                        a_f0           <= dword_a_f0;
                        frame_valid[0] <= 1'b1;
                    end
                    2:
                    begin
                        t_oe              <= dword_29_14;
                        curve_fit_greater <= dword[7];
                        frame_valid[1]    <= 1'b1;
                    end
                    3:
                    begin
                        IODE_sf3       <= dword[23:16];
                        IDOT           <= dword_IDOT;
                        frame_valid[2] <= 1'b1;
                    end
                    endcase
                end
                endcase
            end
        end
    end

endmodule

