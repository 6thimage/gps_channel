`timescale 1ns / 1ps

module ca_gen_single_tb;

    /* inputs */
    reg clk, config_delay, enable;
    reg [4:0] sv_id;
    /* outputs */
    wire code;

    ca_gen_single uut(.clk(clk), .config_delay(config_delay), .enable(enable), .sv_id(sv_id), .code(code));

    always #0.5 clk <= ~clk;

    integer i, complete=0;
    initial
    begin
        /* init inputs */
        clk=1;
        config_delay=0;
        enable=0;

        /* set prn & delay */
        sv_id=30-1;
        config_delay=1;
        #2;

        /* start generation */
        #2;
        config_delay=0;
        enable=1;

        #1024;
        enable=0;

        complete=1;
        #10;
        $finish;
    end

    reg [1022:0] gen_codes;
    integer count=0;
    always @(posedge clk)
    begin
        if(enable)
        begin
            gen_codes[count]=code;
            count=count+1;
        end

        if(complete)
        begin
            for(i=0; i<1023; i=i+1)
            begin
                $display("%d", gen_codes[i]);
            end

            $finish;
        end
    end

endmodule
