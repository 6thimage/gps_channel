module filter_simple #(
    parameter width=10,
    parameter mul_width=10
    )
    (
    input clk,
    input reset,
    input clear,
    /* filter config */
    input signed [width-1:0] constant_c1, constant_c2,
    /* filter inputs */
    input input_valid,
    input signed [width-1:0] filter_in,
    /* filter outputs */
    output reg output_valid,
    output reg signed [width-1:0] filter_out
    );

    initial output_valid=1'b0;

    reg mul_enable=1'b0;
    reg signed [width-1:0] mul_in, mul_const;
    wire signed [width-1:0] mul_out;
    wire mul_valid;
    if(width>16)
    constant_mul_wide #(.mul_width(width+mul_width), .bit_shift(mul_width))
                 mul(.clk(clk), .reset(reset), .enable(mul_enable),
                     .mul_in(mul_in), .mul_const(mul_const),
                     .mul_out(mul_out), .out_valid(mul_valid));
    else
    constant_mul #(.mul_width(width+mul_width), .bit_shift(mul_width))
                 mul(.clk(clk), .reset(reset), .enable(mul_enable),
                     .mul_in(mul_in), .mul_const(mul_const),
                     .mul_out(mul_out), .out_valid(mul_valid));

    /* algorithm:
     * 
     * filter_out = c1*filter_in + c2*last_in
     * last_in = filter_in
     *
     * stages
     *
     * -- stage 0
     * c1*filter_in
     *
     * -- stage 1
     * filter_out = [c1*filter_in]
     * c2*last_in
     *
     * -- stage 2
     * filter_out += [c2*last_in]
     * last_in = filter_in
     */

    reg signed [width-1:0] last_in='d0;

    reg load=1'b0, unload=1'b0;
    always @(posedge clk)
    begin
        if(!reset)
        begin
            output_valid <= 1'b0;
            mul_enable <= 1'b0;
            load <= 1'b0;
            unload <= 1'b0;
        end
        else
        begin
            output_valid <= 1'b0;
            mul_enable <= 1'b0;

            if(input_valid)
            begin
                mul_in <= filter_in;
                mul_const <= constant_c1;
                mul_enable <= 1'b1;

                unload <= 1'b0;
            end

            if(load)
            begin
                mul_in <= last_in;
                mul_const <= constant_c2;
                mul_enable <= 1'b1;
                last_in <= filter_in;
            end

            load <= input_valid;

            /* unloading */
            if(mul_valid)
            begin
                unload <= ~unload;
                if(!unload)
                    filter_out <= mul_out;
                else
                    filter_out <= filter_out + mul_out;

                output_valid <= unload;
            end

            if(clear)
                last_in <= 'd0;
        end
    end

endmodule

