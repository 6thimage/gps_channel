#include "Vdecoder.h"
#include "verilated.h"
#include <cstdio>

vluint64_t time=0;

double sc_time_stamp()
{
    return time;
}

int main(int argc, char **argv, char **env)
{
    FILE *f=fopen("tb_decode.red", "r");

    if(!f)
    {
        puts("Failed to open decoder input file");
        return -1;
    }

    Verilated::commandArgs(argc, argv);
    Vdecoder *top=new Vdecoder;

    top->clk=0;
    top->reset=0;

    while(!Verilated::gotFinish() && !feof(f))
    {
        /* update clock */
        top->clk = !top->clk;

        if(!top->clk)
        {
            top->eval();
            continue;
        }

        /* inputs */
        if(time<100)
        {
            top->reset=0;
        }
        else
        {
            top->reset=1;

            int decode_input;

            if(fscanf(f, "%d", &decode_input)==1)
                top->code_sign_valid=1;
            else
                top->code_sign_valid=0;

            if(decode_input>0)
                top->code_sign_pos=1;
            else
                top->code_sign_pos=0;
        }

        /* run sim */
        top->eval();

        /* outputs */
        if(top->frame_ready)
        {
            printf("frame ready   (%ld)\n", time);
            printf("    integrity level: %s\n", top->enhanced_integrity?"enhanced":"legacy");
            printf("    TOW: %d\n", top->TOW);
            if(top->alert_flag)
                printf("    alert flag set\n");

            if(top->frame_valid&1)
            {
                printf("    week number: %d\n", top->week_number);
                printf("    URA index: %d\n", top->URA_index);
                printf("    SV health: %d\n", top->SV_health_bad);
                printf("    SV health code: %d\n", top->SV_health);
                printf("    IODC: %d\n", top->IODC);
                printf("    t_oc: %d\n", top->t_oc);
                printf("    a_f2: %d\n", top->a_f2);
                printf("    a_f1: %d\n", top->a_f1);
                printf("    a_f0: %d\n", top->a_f0);
            }
            if(top->frame_valid&2)
            {
                printf("    IODE: %d\n", top->IODE_sf2);
                printf("    curve fit %s 4 hours\n", top->curve_fit_greater?">":"=");
                printf("    M_0: %d\n", top->M_0);
                printf("    ecc: %d\n", top->ecc);
                printf("    sqrt_A: %d\n", top->sqrt_A);
                printf("    Delta_n: %d\n", top->Delta_n);
                printf("    t_oe: %d\n", top->t_oe);
                printf("    C_rs: %d\n", top->C_rs);
                printf("    C_uc: %d\n", top->C_uc);
                printf("    C_us: %d\n", top->C_us);
            }
            if(top->frame_valid&4)
            {
                printf("    IODE: %d\n", top->IODE_sf3);
                printf("    Omega_0: %d\n", top->Omega_0);
                printf("    incl_0: %d\n", top->incl_0);
                printf("    C_rc: %d\n", top->C_rc);
                printf("    C_ic: %d\n", top->C_ic);
                printf("    C_is: %d\n", top->C_is);
                printf("    omega: %d\n", top->omega);
                printf("    Omega^dot: %d\n", top->Omega_dot);
                printf("    ID0T: %d\n", top->IDOT);
            }
        }

        /* advance time */
        time++;
    }
    delete top;
    return 0;
}

