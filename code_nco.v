module code_nco(
    input clk,
    input reset,
    /* inputs */
    input [35:0] code_step,
    input tick,
    input [4:0] prn,
    /* slewing */
    input slew,
    input [9:0] slew_code_num,
    output reg slew_complete,
    /* code outputs */
    output reg early,
    output reg prompt,
    output reg late,
    /* prompt code number */
    output reg [9:0] code_num,
    output [35:0] code_num_fine
    );

    /* There is an issue with the code step, as it can be greater than 1 (i.e. if we need to
     * step forward by 2 codes). The issue is that the generator can only step by a code at
     * a time, so this second code update would be missed.
     *
     * To avoid this from happening, the code nco is updated every sample, rather than every
     * 16 samples (at the chipping rate). This means that the code step is 1/16th of a code
     * and nicely removes any issues with the nco trying to double update (essentially, the
     * code output can be incremented by 16 per chip).
     */

    initial code_num=10'd1021;

    always @(*)
    begin
        if(slew_code_num==10'd0)
            slew_complete = (code_num == 10'd1022);
        else
            slew_complete = (code_num == (slew_code_num -1'b1));
    end

    /* On a slightly separate note, the code_num output is not particularly good at the moment,
     * ideally we will need something a little more accurate than a basic integer. It would
     * probably be better to output the code_counter as well, but this will completely screw
     * over the timing. Instead, the correlators could output this information based on an
     * enable signal - this would also allow the attached processor more time to grab the data.
     */

    reg ca_enable=1'b0;
    wire ca_code;
    ca_gen_single ca(.clk(clk), .reset(reset), .enable(ca_enable), .sv_id(prn), .code(ca_code));

    /* epl outputs */
    always @(posedge clk)
    begin
        if(!reset)
        begin
            {early, prompt, late} <= 'd0;
        end
        else
        begin
            if(ca_enable)
            begin
                /* update epl */
                {late, prompt, early} <= {prompt, early, ca_code};
            end
        end
    end

    /* calculate the next code number
     * this is done here so that we can assign it in two places - under normal operation,
     * the code number is updated 1 clock cycle after ca_enable, this is so that it is in
     * time with the change of the epl outputs, however, when slewing we need to update
     * the code number without any delay, which means updating it in time with ca_enable,
     * placing this logic here avoids the synthesis tool working harder to produce only
     * one adder
     */
    reg [9:0] next_code_num;
    always @(*)
    begin
        if(code_num==10'd1022)
            next_code_num = 10'd0;
        else
            next_code_num = code_num + 1'b1;
    end

    /* code stepping */
    reg [35:0] code_counter='d0;
    assign code_num_fine=code_counter;
    always @(posedge clk)
    begin
        if(!reset)
        begin
            /* reset enable */
            ca_enable <= 1'b0;

            code_counter <= 'd0;
            code_num <= 10'd1021;
        end
        else
        begin
            ca_enable <= 1'b0;

            if(slew)
            begin
                if(!slew_complete)
                begin
                    ca_enable <= 1'b1;
                    code_num <= next_code_num;
                end
            end
            else
            begin
                if(tick)
                    {ca_enable, code_counter} <= code_counter + code_step;

                if(ca_enable)
                    code_num <= next_code_num;
            end

        end
    end

endmodule

